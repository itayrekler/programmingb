#include<stdio.h>
#include<stdlib.h>
#include<string>
#include<thread>
#include<fstream>
#include<iostream>
#include<mutex>
#include <chrono>
#include<condition_variable>

using namespace std;

	std::mutex mtx;
	ofstream myfile;
	static int read_count = 0;
	bool isEmpty = true;
	std::condition_variable cv;

	void Reading()
	{
		std::unique_lock<mutex> lk(mtx);
	// critical code start
	if (isEmpty)
	{
		lk.unlock();
	}
	else
	{
		char re[2048];
		read_count++;
		myfile.open("example.txt");
		ifstream inFile("example.txt");
		inFile.getline(re, 2048);
		printf("Reading begin\n");
		cout << re;
		read_count--;
		lk.unlock();
		// Notify_all (condition value)
		cv.notify_all();
		
	}
}

void Writing()
{
	std::unique_lock<mutex> lk(mtx);
	lk.unlock();
	if (read_count < 1)
	{
		lk.lock();
		printf("Writing begin\n");
		isEmpty = false;
		/* writing to file*/
		lk.unlock();
	}
	else
	{
		cv.wait(lk);
	}
}
int main()
{
	std::thread t1(Writing);
	std::thread t2(Reading);
	
	t2.join();
	t1.join();
	system("PAUSE");
	return 0;
}